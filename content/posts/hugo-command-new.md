---
title: "Hugo Commandメモ：New"
date: 2018-03-25T19:45:16+09:00
draft: false
---

##hugo new

###hugo new site
'''hugo new site hoge'''
サイトの新規作成。
実行するとサイト名のディレクトリやconfig.toml、最小限のディレクトリ構造が生成される。

###hugo new [ページ名]
'''hugo new hogehoge'''
ページの新規作成。
実行すると/contentフォルダ内にMarkdownファイルが作成される。